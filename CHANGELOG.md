# Change Log


## Semantic Versioning
https://semver.org/lang/pt-BR/


## [v0.1.0] - 2018-04-22
> feat: Backend - Manager App and Search App / Frontend - Project, Collaborator and Ability CRUD 
- added Backend: Manager App
- added Backend: Search App
- added Database: models and relations (auto create)
- created basic backend structure
- added airbnb TypeScript linter
- added Contributing guid
- added Change log
- added readMe
- added Frontend: Manager and Search Service Template
- added Frontend: Home, Manager and List Component Template
- added Frontend: Project, Collaborator, and Ability CRUD Components
- added Frontend: Project, Collaborator, and Ability Modules and Routers
- added Frontend: Project, Collaborator, and Ability Search and Manage Services
