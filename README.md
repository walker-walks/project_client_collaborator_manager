# Project Client Collaborator Manager App Demo

## Backend
Getting started for development.
run commands below at project root.

```terminal
cd app/demo-server
npm i
node src/bin/install.js
npm run dev
```

App will start at Port: 3000


## Build Test
to test builded app localy, run commands below at project root.

```terminal
sh build.sh
docker-compose up
```


Author: Marcio Koji Carvalho