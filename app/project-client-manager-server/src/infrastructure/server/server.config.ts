export interface IServerConfig {
  readonly port: string;
  readonly env: string;
  readonly isProdMode: boolean;
}
