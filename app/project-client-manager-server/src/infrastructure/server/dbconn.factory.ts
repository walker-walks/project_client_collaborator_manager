import { Sequelize, ISequelizeConfig } from "sequelize-typescript";
import { Client } from "../../domain/client.model";
import { Project } from "../../domain/project.model";
import { Ability } from "../../domain/ability.model";
import { Collaborator } from "../../domain/collaborator.model";
import { Role } from "../../domain/role.model";
import { ProjectCollaborator } from "../../domain/project-collaborator";

export function dbConectionFactory(dbConfig: ISequelizeConfig) {

  const sequelize = new Sequelize(dbConfig);
  sequelize.addModels([
    ProjectCollaborator, Project, Collaborator,
    Ability, Role, Client,
  ]);
  
  return sequelize;
}