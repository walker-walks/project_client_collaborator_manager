import { Component } from "@nestjs/common"
import * as Bluebird from "bluebird";
import { Op } from "sequelize";
// import { Project } from "../../domain/book.model";

@Component()
export class ProjectRepository {

  // findByTitle(title: string): Bluebird<Project[]> {
  //   return Project.findAll({
  //     where: {
  //       title: {
  //         [Op.like]: `%${title}%`,
  //       },
  //     }
  //   });
  // }

  findByAuthor(author: string): Bluebird<Project[]> {
    return Project.findAll({
      where: {
        author: {
          [Op.like]: `%${author}%`,
        },
      }
    });
  }

}
