import {
  Module, Global,
  NestModule,
  MiddlewaresConsumer,
  RequestMethod
} from '@nestjs/common'

import { AppController } from './app.controller'

import { LoggerMiddleware } from './infrastructure/middleware/logger.middleware';
import { CorsMiddlweare } from './infrastructure/middleware/cors.middleware';
import { ManagerModule } from './application/manager/manager.module';
import { SearchModule } from './application/search/search.module';


@Global()
@Module({
  imports: [
    SearchModule,
    ManagerModule,
  ],
})
export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewaresConsumer): void {
    consumer.apply([ LoggerMiddleware, CorsMiddlweare ])
      .forRoutes({ path: '/**', method: RequestMethod.ALL })
  }
}
