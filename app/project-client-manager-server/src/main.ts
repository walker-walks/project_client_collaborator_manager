import { NestFactory } from '@nestjs/core'

import { ApplicationModule } from './app.module'
import * as express from 'express';
import * as path from 'path';

import * as bodyParser from 'body-parser'
import { LoggingInterceptor } from './infrastructure/server/logging.interceptor';
import { SERVERCONFIG, DBCONFIG } from './config/dbconfig';
import { dbConectionFactory } from './infrastructure/server/dbconn.factory';

async function bootstrap() {

  const app = await NestFactory.create(ApplicationModule);
  const sequelize = dbConectionFactory(DBCONFIG);
  await sequelize.sync();

  app.useGlobalInterceptors(new LoggingInterceptor());
  app.use(bodyParser.json());
  app.use(express.static(path.join(__dirname, 'bundle')));


  await app.listen(SERVERCONFIG.port)
}
bootstrap().catch( err => {
  console.log('failed to start app: ', err)
});

console.log(`app started at Port: ${SERVERCONFIG.port}` );
