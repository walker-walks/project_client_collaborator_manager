import { Module } from '@nestjs/common';
import { CollaboratorSearchController } from './controller/collaborator-search.controller';
import { AbilitySearchController } from './controller/ability-search.controller';
import { ClientSearchController } from './controller/client-search.controller';
import { ProjectSearchController } from './controller/project-search.controller';
import { RoleSearchController } from './controller/role-search.controller';


@Module({
  controllers: [
    CollaboratorSearchController,
    AbilitySearchController,
    ClientSearchController,
    ProjectSearchController,
    RoleSearchController
  ],
})
export class SearchModule {}