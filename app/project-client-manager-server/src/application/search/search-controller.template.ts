import { Project } from '../../domain/project.model';
import { Model } from "sequelize-typescript";
import { Body, Param, Get } from "@nestjs/common";

export class SearchTemplate<T> {

  constructor(private classRef: any) {
  }

  @Get()
  async findAll(): Promise<T> {
    return await this.classRef.findAll();
  }

  @Get(':id')
  async findById(@Param('id') id: number | string){
    return await this.classRef.findById(id);
  }

}

