// Common Imports
import { Controller } from "@nestjs/common"
import { SearchAPIv1 } from "../search.constant";
import { SearchTemplate } from "../search-controller.template";

// Independent Imports
import { Collaborator } from "../../../domain/collaborator.model";

@Controller(`${SearchAPIv1}/collaborators`)
export class CollaboratorSearchController extends SearchTemplate<Collaborator>{

  constructor() {
    super(Collaborator)
  }

}
