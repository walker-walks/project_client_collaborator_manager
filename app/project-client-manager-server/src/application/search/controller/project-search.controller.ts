// Common Imports
import { Controller } from "@nestjs/common"
import { SearchAPIv1 } from "../search.constant";
import { SearchTemplate } from "../search-controller.template";

// Independent Imports
import { Project } from "../../../domain/project.model";

@Controller(`${SearchAPIv1}/projects`)
export class ProjectSearchController extends SearchTemplate<Project> {

  constructor() {
    super(Project)
  }

}
