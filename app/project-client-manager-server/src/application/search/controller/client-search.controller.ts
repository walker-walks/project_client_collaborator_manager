// Common Imports
import { Controller } from "@nestjs/common"
import { SearchAPIv1 } from "../search.constant";
import { SearchTemplate } from "../search-controller.template";

// Independent Imports
import { Client } from "../../../domain/client.model";

@Controller(`${SearchAPIv1}/clients`)
export class ClientSearchController extends SearchTemplate<Client>{

  constructor() {
    super(Client)
  }

}
