// Common Imports
import { Controller } from "@nestjs/common"
import { SearchAPIv1 } from "../search.constant";
import { SearchTemplate } from "../search-controller.template";

// Independent Imports
import { Ability } from "../../../domain/ability.model";

@Controller(`${SearchAPIv1}/abilities`)
export class AbilitySearchController extends SearchTemplate<Ability>{

  constructor() {
    super(Ability)
  }

}
