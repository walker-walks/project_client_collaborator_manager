// Common Imports
import { Controller } from "@nestjs/common"
import { SearchAPIv1 } from "../search.constant";
import { SearchTemplate } from "../search-controller.template";

// Independent Imports
import { Role } from "../../../domain/role.model";

@Controller(`${SearchAPIv1}/roles`)
export class RoleSearchController extends SearchTemplate<Role>{

  constructor() {
    super(Role)
  }

}
