import { Model } from "sequelize-typescript";
import { Post, Body, Param, Put, Delete } from "@nestjs/common";

export class ManagerTemplate<T> {

  constructor(private classRef: any) {
  }

  @Post()
  async addCollaborator(@Body() entity: T): Promise<T> {
    return await this.classRef.create(entity);
  }

  @Put(':id')
  async editCollaborator(@Param('id') id: number, @Body() entity: T){
    (entity as any).id = id;
    return await this.classRef.update(entity, {where: {id}});
  }

  @Delete(':id')
  async deleteCollaborator(@Param('id') id: number, @Body() entity: T): Promise<number> {
    return await this.classRef.destroy({where: {id}});
  }
}
