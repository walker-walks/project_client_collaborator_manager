import { Module } from '@nestjs/common'

import { ProjectRepository } from '../../infrastructure/repository/book.repository';
import { CollaboratorManagerController } from './controller/collaborator-manager.controller';
import { ClientManagerController } from './controller/client-manager.controller';
import { ProjectManagerController } from './controller/project-manager.controller';
import { RoleManagerController } from './controller/role-manager.controller';
import { AbilityManagerController } from './controller/ability-manager.controller';

@Module({
  controllers: [
    CollaboratorManagerController,
    AbilityManagerController,
    ClientManagerController,
    ProjectManagerController,
    RoleManagerController
  ],
  components: [],
})
export class ManagerModule {}