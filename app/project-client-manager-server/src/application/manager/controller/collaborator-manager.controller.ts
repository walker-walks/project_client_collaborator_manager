// Common Imports
import { Controller } from "@nestjs/common"
import { ManagerAPIv1 } from "../manager.constant";
import { ManagerTemplate } from "../../manager/manager-controller.template";
// Independent Imports
import { Collaborator } from "../../../domain/collaborator.model";

@Controller(`${ManagerAPIv1}/collaborators`)
export class CollaboratorManagerController extends ManagerTemplate<Collaborator>{

  constructor() {
    super(Collaborator)
  }

}
