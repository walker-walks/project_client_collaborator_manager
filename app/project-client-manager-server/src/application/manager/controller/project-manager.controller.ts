// Common Imports
import { Controller } from "@nestjs/common"
import { ManagerAPIv1 } from "../manager.constant";
import { ManagerTemplate } from "../../manager/manager-controller.template";
// Independent Imports
import { Project } from "../../../domain/project.model";

@Controller(`${ManagerAPIv1}/projects`)
export class ProjectManagerController extends ManagerTemplate<Project> {

  constructor() {
    super(Project)
  }

}
