// Common Imports
import { Controller } from "@nestjs/common"
import { ManagerAPIv1 } from "../manager.constant";
import { ManagerTemplate } from "../../manager/manager-controller.template";
// Independent Imports
import { Role } from "../../../domain/role.model";

@Controller(`${ManagerAPIv1}/roles`)
export class RoleManagerController extends ManagerTemplate<Role>{

  constructor() {
    super(Role)
  }

}
