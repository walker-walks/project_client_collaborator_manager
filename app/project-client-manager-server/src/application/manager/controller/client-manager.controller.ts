// Common Imports
import { Controller } from "@nestjs/common"
import { ManagerAPIv1 } from "../manager.constant";
import { ManagerTemplate } from "../../manager/manager-controller.template";
// Independent Imports
import { Client } from "../../../domain/client.model";

@Controller(`${ManagerAPIv1}/clients`)
export class ClientManagerController extends ManagerTemplate<Client>{

  constructor() {
    super(Client)
  }

}
