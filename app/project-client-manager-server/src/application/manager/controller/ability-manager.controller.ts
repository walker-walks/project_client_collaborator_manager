// Common Imports
import { Controller } from "@nestjs/common"
import { ManagerAPIv1 } from "../manager.constant";
import { ManagerTemplate } from "../../manager/manager-controller.template";
// Independent Imports
import { Ability } from "../../../domain/ability.model";

@Controller(`${ManagerAPIv1}/abilities`)
export class AbilityManagerController extends ManagerTemplate<Ability>{

  constructor() {
    super(Ability)
  }

}
