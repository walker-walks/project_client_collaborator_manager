import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique, HasMany }
from 'sequelize-typescript';
import { Project } from './project.model';

@Table({
  timestamps: false,
  indexes: [{ unique: false, fields: ['name'] }],
})
export class Client extends Model<Client> {

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(45))
  name: string;

  @Column(DataType.STRING(120))
  description: string;

  @Column(DataType.TEXT)
  annotation: string;

  // Relations
  @HasMany( () => Project)
  projects: Project[];
}