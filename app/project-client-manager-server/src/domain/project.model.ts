import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique, BelongsToMany, ForeignKey, HasMany }
from 'sequelize-typescript';
import { Collaborator } from './collaborator.model';
import { ProjectCollaborator } from './project-collaborator';
import { Client } from './client.model';

@Table({
  indexes: [{ unique: false, fields: ['name', 'managerId'] }],
})
export class Project extends Model<Project>{

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(45))
  name: string;

  @Column(DataType.STRING(120))
  description: string;

  @Column(DataType.TEXT)
  annotation: string;

  @Column(DataType.DATE)
  startDate: Date;

  @Column(DataType.DATE)
  endDate: Date;

  @Column(DataType.TINYINT)
  priority: number;

  // Relations
  @ForeignKey( () => Collaborator)
  managerId: number;

  @ForeignKey( () => Client)
  clientId: number;

  @BelongsToMany( () => Collaborator, () => ProjectCollaborator )
  collaborators: Collaborator[];
  
}