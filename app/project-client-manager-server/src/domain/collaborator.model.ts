import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique, ForeignKey, BelongsToMany, HasMany }
from 'sequelize-typescript';
import { Project } from './project.model';
import { ProjectCollaborator } from './project-collaborator';

@Table({
  timestamps: false,
  indexes: [{ unique: false, fields: ['name'] }],
})
export class Collaborator extends Model<Collaborator> {

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(45))
  name: string;

  @Column(DataType.STRING(120))
  description: string;

  @Column(DataType.TEXT)
  annotation: string;

  // Relations
  @BelongsToMany( () => Project, () => ProjectCollaborator )
  projects: Project[];

  @HasMany( () => Project)
  managements: Project[];

}