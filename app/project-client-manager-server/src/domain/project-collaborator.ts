import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique, BelongsToMany, ForeignKey }
from 'sequelize-typescript';
import { Project } from './project.model';
import { Collaborator } from './collaborator.model';

@Table
export class ProjectCollaborator extends Model<ProjectCollaborator> {

  @ForeignKey(()=>Project)
  @Column
  projectId: number;

  @ForeignKey(()=>Collaborator)
  @Column
  collaboratorId: number;
  
}