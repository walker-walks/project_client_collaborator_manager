import { AutoIncrement, Column, DataType, BelongsTo, Model, PrimaryKey, Table, Unique }
from 'sequelize-typescript';

@Table({
  timestamps: false,
  indexes: [{ unique: false, fields: ['name'] }],
})
export class Ability extends Model<Ability> {

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING(45))
  name: string;

  @Column(DataType.STRING(120))
  description: string;

  @Column(DataType.TEXT)
  annotation: string;

}