import { Entity } from '../shared/entity.model';

export class Ability implements Entity {

  id?: number;
  name?: string;
  description?: string;
  annotation?: string;

}
