import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AbilityHomeComponent } from './ability-home/ability-home.component';
import { AbilityListComponent } from './ability-list/ability-list.component';
import { AbilityManagerComponent } from './ability-manager/ability-manager.component';
import { AbilityDetailComponent } from './ability-detail/ability-detail.component';

const routes: Routes = [
  {path: '', component: AbilityHomeComponent, children: [
    {path: '', component: AbilityListComponent, pathMatch: 'full'},
    {path: 'manage', component: AbilityManagerComponent},
    {path: 'manage/:id', component: AbilityManagerComponent},
    {path: ':id', component: AbilityDetailComponent, pathMatch: 'full'},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbilityRoutingModule { }
