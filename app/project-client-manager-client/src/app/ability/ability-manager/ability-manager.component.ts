import { Component, OnInit } from '@angular/core';
import { ManagerComponent } from '../../shared/component/manager.component';
import { Ability } from '../ability.model';
import { AppStatusService } from '../../app-status.service';
import { AbilityManagerService } from './ability-manager.service';

@Component({
  selector: 'app-ability-manager',
  templateUrl: './ability-manager.component.html',
  styleUrls: ['./ability-manager.component.css']
})
export class AbilityManagerComponent extends ManagerComponent<Ability>  {

  entity: Ability = {};

  constructor(
    appStatus: AppStatusService,
    abilityManager: AbilityManagerService,
  ) {
    super(appStatus, abilityManager);
  }

}
