import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ManagerService } from '../../shared/service/manager.service';
import { Ability } from '../ability.model';

@Injectable()
export class AbilityManagerService extends ManagerService<Ability> {
  readonly endpoint = 'abilities';
}
