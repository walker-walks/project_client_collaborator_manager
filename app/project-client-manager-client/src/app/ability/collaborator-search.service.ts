import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SearchService } from '../shared/service/serach.service';

import { Ability } from './ability.model';

@Injectable()
export class AbilitySearchService extends SearchService<Ability> {
  endpoint = 'abilities';
}
