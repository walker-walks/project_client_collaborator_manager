import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../../shared/component/home.component';
import { AppStatusService } from '../../app-status.service';

@Component({
  selector: 'app-ability-home',
  templateUrl: './ability-home.component.html',
  styleUrls: ['./ability-home.component.scss']
})
export class AbilityHomeComponent extends HomeComponent {

  constructor(
    appStatus: AppStatusService
  ) {
    super(appStatus, 'Abilities');
  }

}
