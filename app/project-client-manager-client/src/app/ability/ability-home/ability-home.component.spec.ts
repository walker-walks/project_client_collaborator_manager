import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbilityHomeComponent } from './ability-home.component';

describe('AbilityHomeComponent', () => {
  let component: AbilityHomeComponent;
  let fixture: ComponentFixture<AbilityHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbilityHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbilityHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
