import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';

import { AbilityRoutingModule } from './ability-routing.module';
import { AbilityManagerComponent } from './ability-manager/ability-manager.component';
import { AbilityListComponent } from './ability-list/ability-list.component';
import { AbilityDetailComponent } from './ability-detail/ability-detail.component';
import { AbilityManagerService } from './ability-manager/ability-manager.service';
import { AbilitySearchService } from './collaborator-search.service';
import { AbilityHomeComponent } from './ability-home/ability-home.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    AbilityRoutingModule
  ],
  declarations: [
    AbilityManagerComponent,
    AbilityListComponent,
    AbilityDetailComponent,
    AbilityHomeComponent,
  ],
  providers: [
    AbilityManagerService,
    AbilitySearchService,
  ]
})
export class AbilityModule { }
