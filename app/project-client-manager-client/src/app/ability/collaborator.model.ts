import { Project } from '../project/project.model';
import { Entity } from '../shared/entity.model';

export class Collaborator implements Entity {

  id?: number;
  name?: string;
  description?: string;
  annotation?: string;
  projects?: Project[];
  managements?: Project[];

}
