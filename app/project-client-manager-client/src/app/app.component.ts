import { Component, ViewChild, TemplateRef } from '@angular/core';
import { EventEmitter } from 'events';
import { AppStatusService } from './app-status.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  headerTitle = '';

  constructor(private appStatus: AppStatusService) {
    appStatus.$headerTitle.subscribe(
      title => this.headerTitle = title
    );
  }

  isCollapsed = true;
  isReverseArrow = false;
  width = 200;

  triggerTemplate = null;
  @ViewChild('trigger') customTrigger: TemplateRef<void>;

  /** custom trigger can be TemplateRef **/
  changeTrigger(): void {
    this.triggerTemplate = this.customTrigger;
  }
}
