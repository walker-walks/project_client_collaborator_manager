import { OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';
import { SearchService } from '../service/serach.service';
import { Entity } from '../entity.model';

export class ListComponent<T extends Entity> implements OnInit {

  entities: any[] = [];

  constructor(
    private appStatus: AppStatusService,
    private searchService: SearchService<T>,
  ) { }

  ngOnInit() {
    this.searchService.findAll().subscribe(
      datas => this.entities = datas,
      err => console.log('err', err)
    );
    this.appStatus.setBreadCrumb(undefined);
  }

}
