import { OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';

export class HomeComponent {

  locations = undefined;

  constructor(
    appStatus: AppStatusService, protected title: string,
  ) {
    appStatus.setHeaderTitle(title);
    appStatus.$breadCrumb.subscribe(
      locations => this.locations = locations
    );
    appStatus.setBreadCrumb(undefined);
  }

}
