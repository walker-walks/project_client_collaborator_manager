import { OnInit } from '@angular/core';
import { Entity } from '../entity.model';
import { AppStatusService } from '../../app-status.service';
import { ManagerService } from '../service/manager.service';

export class ManagerComponent<T extends Entity> implements OnInit {

  entity: T;
  breadCrumb: string[] | undefined;

  constructor(
    protected readonly appStatus: AppStatusService,
    protected readonly managerService: ManagerService<T>,
  ) {

  }

  ngOnInit() {
    this.appStatus.setBreadCrumb(this.breadCrumb);
  }

  submit() {
    console.log('submit', this.entity);
    this.managerService.create(this.entity).subscribe(
      data => console.log('done', data),
      err => console.log('err', err)
    );
  }

}
