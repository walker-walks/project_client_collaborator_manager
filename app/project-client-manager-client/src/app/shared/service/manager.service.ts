import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Entity } from '../entity.model';

@Injectable()
export class ManagerService<T extends Entity> {

  readonly ManagerAPIv1 = 'http://localhost:3000/api/v1/manage/';
  endpoint = '';
  get APIv1(): string {
    return `${this.ManagerAPIv1}${this.endpoint}`;
  }

  constructor(private http: HttpClient) {

  }

  create(project: T): Observable<T> {
    return this.http.post<T>(`${this.APIv1}`, project);
  }

  update(project: T): Observable<T> {
    return this.http.put<T>(`${this.APIv1}/${project.id}`, project);
  }

  delete(project: T): Observable<T> {
    return this.http.delete<T>(`${this.APIv1}/${project.id}`, project);
  }

}
