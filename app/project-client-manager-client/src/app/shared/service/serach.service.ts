import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Entity } from '../entity.model';

@Injectable()
export class SearchService<T extends Entity> {

  readonly SearchAPIv1 = 'http://localhost:3000/api/v1/search/';
  endpoint = '';
  get APIv1(): string {
    return `${this.SearchAPIv1}${this.endpoint}`;
  }

  constructor(private http: HttpClient) {

  }

  findAll(): Observable<any> {
    return this.http.get(`${this.APIv1}`);
  }

  findById(id: number | string): Observable<T> {
    return this.http.get<T>(`${this.APIv1}/${id}`);
  }

}
