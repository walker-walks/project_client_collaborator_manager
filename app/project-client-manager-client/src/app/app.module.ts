import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// Rxjs
import './shared/rxjs.import';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AppStatusService } from './app-status.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgZorroAntdModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    AppStatusService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
