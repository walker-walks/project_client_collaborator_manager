import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';

import { CollaboratorRoutingModule } from './collaborator-routing.module';
import { CollaboratorManagerComponent } from './collaborator-manager/collaborator-manager.component';
import { CollaboratorListComponent } from './collaborator-list/collaborator-list.component';
import { CollaboratorDetailComponent } from './collaborator-detail/collaborator-detail.component';
import { CollaboratorHomeComponent } from './collaborator-home/collaborator-home.component';
import { CollaboratorManagerService } from './collaborator-manager/collaborator-manager.service';
import { CollaboratorSearchService } from './collaborator-search.service';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    CollaboratorRoutingModule
  ],
  declarations: [
    CollaboratorManagerComponent,
    CollaboratorListComponent,
    CollaboratorDetailComponent,
    CollaboratorHomeComponent,
  ],
  providers: [
    CollaboratorManagerService,
    CollaboratorSearchService,
  ]
})
export class CollaboratorModule { }
