import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaboratorHomeComponent } from './collaborator-home/collaborator-home.component';
import { CollaboratorListComponent } from './collaborator-list/collaborator-list.component';
import { CollaboratorManagerComponent } from './collaborator-manager/collaborator-manager.component';
import { CollaboratorDetailComponent } from './collaborator-detail/collaborator-detail.component';

const routes: Routes = [
  {path: '', component: CollaboratorHomeComponent, children: [
    {path: '', component: CollaboratorListComponent, pathMatch: 'full'},
    {path: 'manage', component: CollaboratorManagerComponent},
    {path: 'manage/:id', component: CollaboratorManagerComponent},
    {path: ':id', component: CollaboratorDetailComponent, pathMatch: 'full'},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaboratorRoutingModule { }
