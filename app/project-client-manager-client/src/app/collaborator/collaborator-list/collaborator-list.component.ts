import { Component, OnInit } from '@angular/core';
import { ListComponent } from '../../shared/component/list.component';
import { Collaborator } from '../collaborator.model';
import { AppStatusService } from '../../app-status.service';
import { CollaboratorSearchService } from '../collaborator-search.service';

@Component({
  selector: 'app-collaborator-list',
  templateUrl: './collaborator-list.component.html',
  styleUrls: ['./collaborator-list.component.css']
})
export class CollaboratorListComponent extends ListComponent<Collaborator> {

  constructor(
    appStatus: AppStatusService,
    collaboratorSearch: CollaboratorSearchService,
  ) {
    super(appStatus, collaboratorSearch);
  }


}
