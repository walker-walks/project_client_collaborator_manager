import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Collaborator } from '../collaborator.model';
import { ManagerService } from '../../shared/service/manager.service';

@Injectable()
export class CollaboratorManagerService extends ManagerService<Collaborator> {
  readonly endpoint = 'collaborators';
}
