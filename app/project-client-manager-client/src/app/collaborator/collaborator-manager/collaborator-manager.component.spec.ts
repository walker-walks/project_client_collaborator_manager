import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorManagerComponent } from './collaborator-manager.component';

describe('CollaboratorManagerComponent', () => {
  let component: CollaboratorManagerComponent;
  let fixture: ComponentFixture<CollaboratorManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaboratorManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
