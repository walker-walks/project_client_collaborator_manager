import { Component, OnInit } from '@angular/core';
import { Collaborator } from '../collaborator.model';
import { AppStatusService } from '../../app-status.service';
import { CollaboratorManagerService } from './collaborator-manager.service';
import { ManagerComponent } from '../../shared/component/manager.component';

@Component({
  selector: 'app-collaborator-manager',
  templateUrl: './collaborator-manager.component.html',
  styleUrls: ['./collaborator-manager.component.css']
})
export class CollaboratorManagerComponent extends ManagerComponent<Collaborator> {

  entity: Collaborator = {};

  constructor(
    appStatus: AppStatusService,
    collaboratorManager: CollaboratorManagerService,
  ) {
    super(appStatus, collaboratorManager);
  }

}
