import { Component, OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';
import { HomeComponent } from '../../shared/component/home.component';

@Component({
  selector: 'app-collaborator-home',
  templateUrl: './collaborator-home.component.html',
  styleUrls: ['./collaborator-home.component.scss']
})
export class CollaboratorHomeComponent extends HomeComponent {

  constructor(
    appStatus: AppStatusService
  ) {
    super(appStatus, 'Collaboratos');
  }

}
