import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SearchService } from '../shared/service/serach.service';
import { Collaborator } from './collaborator.model';

@Injectable()
export class CollaboratorSearchService extends SearchService<Collaborator> {
  endpoint = 'collaborators';
}
