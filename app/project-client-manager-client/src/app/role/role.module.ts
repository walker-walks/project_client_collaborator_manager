import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleRoutingModule } from './role-routing.module';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleManagerComponent } from './role-manager/role-manager.component';

@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule
  ],
  declarations: [RoleDetailComponent, RoleListComponent, RoleManagerComponent]
})
export class RoleModule { }
