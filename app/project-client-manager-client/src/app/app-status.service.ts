import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AppStatusService {
  $headerTitle = new Subject<string>();
  $breadCrumb = new Subject<string[] | undefined>();

  constructor() {
    this.setHeaderTitle('Projects');
  }

  setHeaderTitle(title: string): void {
    this.$headerTitle.next(title);
  }

  setBreadCrumb(locations: string[] | undefined) {
    this.$breadCrumb.next(locations);
  }
}
