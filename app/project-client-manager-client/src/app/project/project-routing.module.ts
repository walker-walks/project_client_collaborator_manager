import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectManagerComponent } from './project-manager/project-manager.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectHomeComponent } from './project-home/project-home.component';

const routes: Routes = [
  {path: '', component: ProjectHomeComponent, children: [
    {path: '', component: ProjectListComponent, pathMatch: 'full'},
    {path: 'manage', component: ProjectManagerComponent},
    {path: 'manage/:id', component: ProjectManagerComponent},
    {path: ':id', component: ProjectDetailComponent, pathMatch: 'full'},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
