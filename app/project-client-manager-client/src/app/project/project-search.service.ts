import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Project } from './project.model';

@Injectable()
export class ProjectSearchService {

  readonly projectManagerV1 = 'http://localhost:3000/api/v1/search/projects';

  constructor(private http: HttpClient) {

  }

  findAll(): Observable<any> {
    return this.http.get(this.projectManagerV1);
  }

  findById(id: number | string): Observable<Project> {
    return this.http.get<Project>(`${this.projectManagerV1}/${id}`);
  }

}
