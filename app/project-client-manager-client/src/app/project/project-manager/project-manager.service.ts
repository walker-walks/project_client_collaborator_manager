import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Project } from '../project.model';

@Injectable()
export class ProjectManagerService {
  readonly projectManagerV1 = 'http://localhost:3000/api/v1/manage/projects';

  constructor(private http: HttpClient) {

  }

  createProject(project: Project): Observable<Project> {
    return this.http.post<Project>(this.projectManagerV1, project);
  }
}
