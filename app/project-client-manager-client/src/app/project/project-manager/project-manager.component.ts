import { Component, OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';
import { ProjectManagerService } from './project-manager.service';
import { catchError, retry } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Project } from '../project.model';

@Component({
  selector: 'app-project-manager',
  templateUrl: './project-manager.component.html',
  styleUrls: ['./project-manager.component.css']
})
export class ProjectManagerComponent implements OnInit {

  project: Project = {
    name: '',
    priority: 0,
    description: '',
    managerId: 0,
    annotation: '',
    // clientName: ''
  };
  constructor(
    private appStatus: AppStatusService,
    private projectManger: ProjectManagerService,
  ) {

  }
  ngOnInit() {
    this.appStatus.setBreadCrumb(['Manager', 'New Project']);
  }

  submit() {
    console.log('submit', this.project);
    this.projectManger.createProject(this.project).subscribe(
      data => console.log('done', data),
      err => console.log('err', err)
    );
  }

  // private handleError(error: HttpErrorResponse); {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // return an ErrorObservable with a user-facing error message
  //   return new ErrorObservable(
  //     'Something bad happened; please try again later.');
  // }

}
