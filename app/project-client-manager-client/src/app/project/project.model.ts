import { Collaborator } from '../collaborator/collaborator.model';
import { Entity } from '../shared/entity.model';

export class Project implements Entity {

  id?: number;
  name?: string;
  description: string;
  annotation?: string;
  startDate?: Date;
  endDate?: Date;
  priority?: number;
  managerId?: number;
  collaborators?: Collaborator[];
}
