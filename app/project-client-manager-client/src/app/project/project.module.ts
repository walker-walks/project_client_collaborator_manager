import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';

import { ProjectRoutingModule } from './project-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectManagerComponent } from './project-manager/project-manager.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectHomeComponent } from './project-home/project-home.component';
import { ProjectManagerService } from './project-manager/project-manager.service';
import { ProjectSearchService } from './project-search.service';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ProjectRoutingModule,
    NgZorroAntdModule,
  ],
  declarations: [
    ProjectDetailComponent,
    ProjectManagerComponent,
    ProjectListComponent,
    ProjectHomeComponent,
  ],
  providers: [
    ProjectManagerService,
    ProjectSearchService,
  ]
})
export class ProjectModule { }
