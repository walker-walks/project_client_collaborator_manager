import { Component, OnInit } from '@angular/core';
import { Project } from '../project.model';
import { ProjectSearchService } from '../project-search.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  project: Project;
  constructor(
    private route: ActivatedRoute,
    private projectSerach: ProjectSearchService,
  ) {

  }

  ngOnInit() {
    this.route.params
    .map( (params): number => params.id)
    .switchMap(id => this.projectSerach.findById(id))
    .subscribe(
      project => this.project = project
    );
  }

}
