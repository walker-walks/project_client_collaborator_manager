import { Component, OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';
import { ProjectSearchService } from '../project-search.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  projects: any[] = [];

  constructor(
    private appStatus: AppStatusService,
    private projectSearch: ProjectSearchService,
  ) { }

  ngOnInit() {
    this.projectSearch.findAll().subscribe(
      datas => this.projects = datas,
      err => console.log('err', err)
    );
    this.appStatus.setBreadCrumb(undefined);
  }

}
