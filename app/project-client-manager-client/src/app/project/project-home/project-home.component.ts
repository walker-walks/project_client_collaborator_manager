import { Component, OnInit } from '@angular/core';
import { AppStatusService } from '../../app-status.service';

@Component({
  selector: 'app-project-home',
  templateUrl: './project-home.component.html',
  styleUrls: ['./project-home.component.scss']
})
export class ProjectHomeComponent implements OnInit {

  locations = undefined;

  constructor(
    appStatus: AppStatusService
  ) {
    appStatus.setHeaderTitle('Projects');
    appStatus.$breadCrumb.subscribe(
      locations => this.locations = locations
    );
    appStatus.setBreadCrumb(undefined);
  }

  ngOnInit() {
  }

}
