import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project/project-list/project-list.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'projects'},
  {path: 'projects', loadChildren: 'app/project/project.module#ProjectModule'},
  {path: 'collaborators', loadChildren: 'app/collaborator/collaborator.module#CollaboratorModule'},
  {path: 'abilities', loadChildren: 'app/ability/ability.module#AbilityModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
